
""" test example file for drv8825.py

 Note: Maximum step frequency is limited by the stepper motor physics.
       In microstepping-mode high(er) step frequencies may work fine!
"""

import sys
from time import sleep_ms
import drv8825_setup

def run_test(mot, enc, button):

    def action(revs, mode, speed):
        input(f"Press <Enter> to continue {revs=} {mode=} {speed=} ")
        mot.revolutions(revs, mode, speed)
        while mot.progress() > 0:       # if not all steps taken
            if button():            # button pressed
                mot.disable()
                break
            sleep_ms(50)                # wait
        return abs(revs * mot.steps_per_revolution - mot.progress())

    try:
        # full step variants, different speeds and directions
        # <number of steps>, <step type>, <step frequency>
        action( 1, "Full", 50)
        action(-1, "Full", 100)
        action( 5, "Full", 400)
        action(-5, "Full", 600)
        # microstepping variants
        action( 1, "Half",  500)
        action(-1, "1/4",  1000)
        action( 5, "1/8",  2000)
        action(-5, "1/16", 4000)
        action( 3, "1/32", 8000)
    except KeyboardInterrupt:
        print("Interrupted from Keyboard")

    mot.disable()


# ===================MAIN===============================

print("TEST START")
if (enc := drv8825_setup.setup_rotary()) is None:
    print("No rotary encoder")
    sys.exit()
if (mot := drv8825_setup.setup_stepper()) is None:
    print("No stepper driver")
    sys.exit()
button, _, _ = drv8825_setup.setup_switches()
run_test(mot, enc, button)
print("TEST END")

# =====================END===========================
