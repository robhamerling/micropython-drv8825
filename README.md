<<<<<<< HEAD
# Micropython library for DRV8825 stepper motor controller
# Version 0.3, September 2023
=======

- # Micropython library for DRV8825 stepper motor controller
# Version 0.2, August 2023
>>>>>>> de7014235447a1d73ed0448d885c96067301087f

This project using Micropython programming language and a microcontroller
supporting **Micropython** (like an **ESP32** or **RP2 Pico**) to control
a stepper motor by a DRV8825 chip.

***This repository is under construction!***

## Hardware

- ESP32 microcontroller development board (frequently called NODEMCU)
    - with 4 MB SRAM and preferrably extra 4 MB or more PSRAM or SPIRAM
  A board with DRV8825 chip like:
    - https://www.tinytronics.nl/shop/en/mechanics-and-actuators/
    motor-controllers-and-drivers/stepper-motor-controllers-and-drivers/drv8825-motor-driver-module
    - https://www.pololu.com/product/2133

The ESP32 or RP2 Pico board is supposed to be powered from a USB port of a PC.


## Software

Driver:
- drv8825.py            - class libary for the DRV8825

Samples:
- drv8825_followme.py   - example of servo-like application with rotary encoder
- drv8825_speeds.py     - example of using 'freerun' method
- driv8825_steps.py     - example of using 'steps' method
- drv8825_turns.py      - example of using 'revolutions' method
- drv8825_setup.py      - common setup for all samples for ESP32 or RP2 Pico
- switch.py             - class for switches and buttons
- encoder_portable.py   - class for rotary encoders (author Peter Hinch)


## Documentation

Subdirectory **doc** contains datasheet and wiring diagrams for ESP32 and RP2040 pico.


## Usage

The driver supports 2 ways of using a stepper motor:
- free running at a certain stepping rate
- performing a specific number of steps
In all cases the stepping can be interrupted at any time

See the examples for possible applications.

## Summary of changes
<<<<<<< HEAD
 0.1 Using hardware PWM (non-blocking) or Software PWM (blocking)
 0.2 Fundamental change: using interval timer for all stepping modes.
     Stepping mode now to be specified as number of microsteps: 1,2,4,8,16,32.
     Added 'stop' method: keep motor fixed in place.
 0.3 Minor updates
=======
- 0.1 Using hardware PWM (non-blocking) or Software PWM (blocking)
- 0.2 Fundamental change: using interval timer for all stepping modes.
>>>>>>> de7014235447a1d73ed0448d885c96067301087f


.
