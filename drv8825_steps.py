
""" test example file for drv8825.py

 Note: Maximum step frequency is limited by the stepper motor physics.
       In microstepping-mode high(er) step frequencies may work fine!
"""

import sys
from time import sleep_ms
import drv8825_setup                    # wiring of DRV8825 and rotary encoder

def run_test(mot, button):

    def action(steps, mode, speed):
        input(f"Press <Enter> to continue {steps=} {mode=} {speed=} ")
        mot.steps(steps, mode, speed)
        while mot.progress() > 0:       # if not all steps taken
            if button():                # button pressed
                mot.disable()
                break
            sleep_ms(50)                # wait
        return abs(steps - mot.progress())

    try:
        # full step variants, different speeds and directions
        # <number of steps>, <step type>, <step frequency>
        action( 100, "Full", 50)
        action(-100, "Full", 100)
        action( 100, "Full", 600)
        action( -50, "Full", 100)
        action( -50, "Full", 400)
        # microstepping variants
        action(  400, "Half",  500)
        action( -800, "1/4",  1000)
        action( 1600, "1/8",  2000)
        action(-3200, "1/16", 4000)
        action( 6400, "1/32", 8000)
    except KeyboardInterrupt:
        print("Interrupted from Keyboard")

    mot.disable()


# ===================MAIN===============================

print("TEST START")
if (mot := drv8825_setup.setup_stepper()) is None:
    print("No stepper driver")
    sys.exit()
(button, _,  _) = drv8825_setup.setup_switches()
run_test(mot, button)
print("TEST END")

# =====================END===========================
